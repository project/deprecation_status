<?php

namespace Drupal\Tests\deprecation_status\Functional;

use Drupal\Core\Url;
use Drupal\Tests\BrowserTestBase;

/**
 * Smoke tests for deprecation status.
 *
 * @group deprecation_status
 */
class DeprecationStatusSmokeTest extends BrowserTestBase {

  /**
   * Modules to install.
   *
   * @var array
   */
  protected static $modules = ['deprecation_status'];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Tests access without permission.
   */
  public function testAccessUnprivileged() {
    // Assuming visitors at least have access content permission.
    $this->drupalLogin($this->drupalCreateUser(['access content']));

    $this->drupalGet(Url::fromRoute('deprecation_status.summary'));
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains('regular report to check contributed projects for deprecated API uses and major version compatibility');

    $this->drupalGet(Url::fromRoute('deprecation_status.projects_form'));
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains('Filter projects');
    $this->assertSession()->pageTextContains('pathauto');
    $this->assertSession()->pageTextContains('Compatibility issues');

    $this->drupalGet(Url::fromRoute('deprecation_status.errors_form'));
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains('Filter errors');
    $this->assertSession()->pageTextContains('Drupal API, rector covered');

    $this->drupalGet(Url::fromRoute('deprecation_status.error', ['error_index' => 1]));
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains('Error details for');

    $this->drupalGet(Url::fromRoute('deprecation_status.project', ['machine_name' => 'pathauto']));
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains('pathauto');
    $this->assertSession()->pageTextContains('Berdir');

    $this->drupalGet(Url::fromRoute('deprecation_status.charts'));
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains('Percentage of contributed projects by next step in usage groups');
    $this->assertSession()->pageTextContains('All drupal.org contributed projects by next step over time');

    $this->drupalGet(Url::fromRoute('deprecation_status.update'));
    $this->assertSession()->statusCodeEquals(403);
  }

  /**
   * Tests access with user that has the correct permission.
   */
  public function testAccessPrivileged() {
    $this->drupalLogin($this->drupalCreateUser(['access administration pages']));
    $this->drupalGet(Url::fromRoute('deprecation_status.update'));
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains('Shipped dataset');
    $this->assertSession()->pageTextContains('Current live dataset');
    $this->assertSession()->pageTextContains('Online dataset');

    $this->submitForm([], 'Download and adopt online dataset');
    $this->assertSession()->pageTextContains('Updated dataset successfully.');
  }

}
