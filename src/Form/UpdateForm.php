<?php

namespace Drupal\deprecation_status\Form;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\deprecation_status\DataSource;
use GuzzleHttp\Exception\RequestException;

class UpdateForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'deprecation_status_update_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Always add a reset possibility just in case.
    list($shipped_data_id, $shipped_data_date) = DataSource::getShippedMetaData();
    list($d11_shipped_data_id, $d11_shipped_data_date) = DataSource::getShippedMetaData(11);
    list($data_id, $data_date) = DataSource::getMetaData();
    list($d11_data_id, $d11_data_date) = DataSource::getMetaData(11);

    $form['shipped_data'] = [
      '#type' => 'item',
      '#title' => $this->t('Shipped dataset'),
      '#markup' => $this->t('#@data_id at @data_date', ['@data_id' => $shipped_data_id . '/' . $d11_shipped_data_id, '@data_date' => $shipped_data_date . '/' . $d11_shipped_data_date]),
    ];
    $form['reset'] = [
      '#type' => 'submit',
      '#name' => 'reset',
      '#value' => $this->t('Reset dataset to shipped state'),
    ];
    $form['live_data'] = [
      '#type' => 'item',
      '#title' => $this->t('Current live dataset'),
      '#markup' => $this->t('#@data_id at @data_date', ['@data_id' => $data_id . '/' . $d11_data_id, '@data_date' => $data_date . '/' . $d11_data_date]),
    ];

    $client = \Drupal::httpClient();
    try {
      // Get Drupal 10 metadata values.
      $response = $client->get('https://git.drupalcode.org/project/deprecation_status/raw/8.x-6.x/data/metadata.csv?' . time());
      $data = (string) $response->getBody();
      if (!empty($data)) {
        list($new_data_id, $new_data_date) = explode(';', trim($data));
      }
      else {
        $form['error'] = [
          '#type' => 'markup',
          '#markup' => '<p>Drupal 10 metadata format error while fetching current dataset metadata.</p>',
        ];
        return $form;
      }

      // Get Drupal 11 metadata values.
      $response = $client->get('https://git.drupalcode.org/project/deprecation_status/raw/8.x-6.x/data/11_metadata.csv?' . time());
      $data = (string) $response->getBody();
      if (!empty($data)) {
        list($d11_new_data_id, $d11_new_data_date) = explode(';', trim($data));
      }
      else {
        $form['error'] = [
          '#type' => 'markup',
          '#markup' => '<p>Drupal 11 metadata format error while fetching current dataset metadata.</p>',
        ];
        return $form;
      }
    }
    catch (RequestException $e) {
      $form['error'] = [
        '#type' => 'markup',
        '#markup' => '<p>HTTP error while fetching current dataset metadata.</p>',
      ];
      return $form;
    }

    $form['online_data'] = [
      '#type' => 'item',
      '#title' => $this->t('Online dataset'),
      '#markup' => $this->t('#@data_id at @data_date', ['@data_id' => $new_data_id . '/' . $d11_new_data_id, '@data_date' => $new_data_date . '/' . $d11_new_data_date]),
    ];
    $form['update'] = [
      '#type' => 'submit',
      '#name' => 'update',
      '#value' => $this->t('Download and adopt online dataset'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $triggering_element = $form_state->getTriggeringElement();
    $button_name = $triggering_element['#name'];
    if ($button_name == 'reset') {
      $result = DataSource::resetDataFiles();
      $this->messenger()->addMessage('Deleted ' . (int) $result . ' dataset files.');
      Cache::invalidateTags(['deprecation_status']);
    }
    elseif ($button_name == 'update') {
      $result = DataSource::updateDataFiles();
      if ($result === TRUE) {
        $this->messenger()->addMessage('Updated dataset successfully.');
        Cache::invalidateTags(['deprecation_status']);
      }
      else {
        $this->messenger()->addError($result);
      }
    }
  }

}
