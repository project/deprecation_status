<?php

namespace Drupal\deprecation_status\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\deprecation_status\DataSource;

class SummaryController extends ControllerBase {

  public function summaryPage($target_version = 10) {
    // Make sure target_version is either 11 or 10.
    $target_version = ($target_version == 11) ? 11 : 10;
    $target_version_suffix = ($target_version == 11) ? '11' : '';

    $build = ['#cache' => ['tags' => ['deprecation_status']]];

    list($data_id, $data_date) = DataSource::getMetaData($target_version);
    $file = fopen(DataSource::getFullPath('next_steps_specific.csv', $target_version), 'r');
    $last_line = '';
    while ($line = fgetcsv($file, 0, ";")) {
      $last_line = $line;
    }
    fclose($file);
    // Time;"Make tagged release available";"Improve stability of the release";
    // "Stable, congrats";"Fix info.yml/composer.json errors found";
    // "Run Rector to fix all errors";"Run Rector to fix some errors";
    // "Manually review and fix errors";"Resolve fatal error from PHPStan";
    // "Fix invalid PHPStan results";"Fix malformed info.yml file";
    // "Fix machine name issue";"Fix composer installation issue";
    // "Fix PHP fatal error";"Fix PHP environment constraints if possible";
    // "Check uncategorized error leading to missing results";
    // "Check uncategorized error leading to empty results";
    // "Project is abandoned or obsolete"

    // 1699301246;  87;26;103;  4664;0;0;1488;  94;2;3;95;318;15;0;88;60; 55

    list($time, $next_tagged, $next_stability, $next_stable, $e1, $e2, $e3, $e4, $p1, $p2, $p3, $p4, $p5, $p6, $p7, $p8, $p9, $gone) = $last_line;
    $errors = $e1 + $e2 + $e3 + $e4;
    $pre_scan = $p1 + $p2 + $p3 + $p4 + $p5 + $p6 + $p7 + $p8 + $p9;
    $total = $pre_scan + $errors + $gone + $next_tagged + $next_stability + $next_stable;

    $build['intro'] = [
      '#type' => 'markup',
      '#markup' => '<p><a href="https://www.drupal.org/association">The Drupal Association</a> is running a regular report to check contributed projects for deprecated API uses and major version compatibility. This summary and the detail pages are using the raw data from that report to help act on the results. The current displayed dataset is #' .  $data_id . ' from ' . $data_date . '. <a href="https://git.drupalcode.org/project/project_analysis/-/tree/' . ($target_version == 11 ? '11-' : '') . $data_id . '">All the (very, very) raw source data is available</a>. Out of the <strong>' . $total . ' projects analyzed</strong>, there are three big categories of results: pre-scanning problems that prevent us from looking for deprecated API uses; scanning results and project release status once the code is marked compatible.</p>'
    ];

    $build['chart'] = [
        '#children' => <<<SCRIPTEND
  <script src="https://cdn.jsdelivr.net/npm/chart.js@3.6.2/dist/chart.min.js"></script>
  <div id="charts">
   <canvas id="totalNextSteps"></canvas>
  </div>
  <script type="text/javascript">
  var ctx = document.getElementById('totalNextSteps').getContext('2d');
  var chart = new Chart(ctx, {
    type: 'bar',
    data: {
      labels: ['Status'],
      datasets: [
        {
          label: 'Project is abandoned or obsolete',
          data: [$gone],
          backgroundColor: "#000000"
        },
        {
          label: 'Resolve pre-scanning errors',
          data: [$pre_scan],
          backgroundColor: "#9B2226"
        },
        {
          label: 'Fix deprecation errors found',
          data: [$errors],
          backgroundColor: "#EE9B00"
        },
        {
          label: 'Make tagged release available',
          data: [$next_tagged],
          backgroundColor: "#94D2BD"
        },
        {
          label: 'Improve stability of the release',
          data: [$next_stability],
          backgroundColor: "#0A9396"
        },
        {
          label: 'Stable, congrats',
          data: [$next_stable],
          backgroundColor: "#005F73"
        }
      ]
    },
    options: {
      aspectRatio: window.innerWidth < 800 ? 3 : 6,
      responsive: true,
      indexAxis: 'y',
      scales: {
        x: {
          stacked: true,
          max: $total,
        },
        y: {
          stacked: true,
        }
      },
      plugins: {
        title: {
          display: true,
          text: '$total projects by next steps',
          position: 'top'
        },
        legend: {
          display: true,
          onClick: null
        }
      }
    }
  });
</script>
SCRIPTEND];

    $file = fopen(DataSource::getFullPath('next_steps_help.csv', $target_version), 'r');
    $steps_help = [];
    $i = 0;
    while ($line = fgetcsv($file, 0, ";")) {
      $i++;
      if (($i === 1)) {
        continue;
      }
      $steps_help[$line[0]] = $line[1];
    }
    fclose($file);

    $next_step = 'Project is abandoned or obsolete';
    $build['gone'] = [
      '#type' => 'markup',
      '#markup' => '<h2><a href="' . Url::fromRoute('deprecation_status.projects_form' . $target_version_suffix, [], ['query' => ['next_step' => '-- ' . $next_step]])->toString() . '">' . $next_step . ': ' . $gone . ' projects</a></h2><p>' . $steps_help['-- ' . $next_step] . '</p>'
    ];
    $next_step = 'Resolve pre-scanning errors';
    $build['pre_scanning'] = [
      '#type' => 'markup',
      '#markup' => '<h2><a href="' . Url::fromRoute('deprecation_status.projects_form' . $target_version_suffix, [], ['query' => ['next_step' => $next_step]])->toString() . '">' . $next_step . ': ' . $pre_scan . ' projects</a></h2><p>' . $steps_help[$next_step] . '</p>'
    ];
    $next_step = 'Fix deprecation errors found';
    $build['fix'] = [
      '#type' => 'markup',
      '#markup' => '<h2><a href="' . Url::fromRoute('deprecation_status.projects_form' . $target_version_suffix, [], ['query' => ['next_step' => $next_step]])->toString() . '">' . $next_step . ': ' . $errors . ' projects</a></h2><p>' . $steps_help[$next_step] . '</p>'
    ];
    $next_step = 'Make tagged release available';
    $build['next_tagged'] = [
      '#type' => 'markup',
      '#markup' => '<h2><a href="' . Url::fromRoute('deprecation_status.projects_form' . $target_version_suffix, [], ['query' => ['next_step' => '-- ' . $next_step]])->toString() . '">' . $next_step . ': ' . $next_tagged . ' projects</a></h2><p>' . $steps_help['-- ' . $next_step] . '</p>'
    ];
    $next_step = 'Improve stability of the release';
    $build['next_stability'] = [
      '#type' => 'markup',
      '#markup' => '<h2><a href="' . Url::fromRoute('deprecation_status.projects_form' . $target_version_suffix, [], ['query' => ['next_step' => '-- ' . $next_step]])->toString() . '">' . $next_step . ': ' . $next_stability . ' projects</a></h2><p>' . $steps_help['-- ' . $next_step] . '</p>'
    ];
    $next_step = 'Stable, congrats';
    $build['next_stable'] = [
      '#type' => 'markup',
      '#markup' => '<h2><a href="' . Url::fromRoute('deprecation_status.projects_form' . $target_version_suffix, [], ['query' => ['next_step' => '-- ' . $next_step]])->toString() . '">' . $next_step . ': ' . $next_stable . ' projects</a></h2><p>' . $steps_help['-- ' . $next_step] . '</p>'
    ];
    return $build;
  }

}
