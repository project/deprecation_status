# Contrib deprecation status

This project is both a module displaying deprecation status information and a
set of scripts to generate the insight data. The '8.x-1.x' branch contains the
module with the latest data set while the 'script' branch contains the script
to produce the data.

# Installation

Just install like a regular Drupal 8 module. Go to /deprecation_status to use
the user interface. This UI is intended for developers and contribution events.
